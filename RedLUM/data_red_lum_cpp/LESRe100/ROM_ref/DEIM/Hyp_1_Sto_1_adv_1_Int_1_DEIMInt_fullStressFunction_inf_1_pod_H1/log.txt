/*---------------------------------------------------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2212                                  |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
Build  : _70874860-20230612 OPENFOAM=2212 patch=230612 version=2212
Arch   : "LSB;label=32;scalar=64"
Exec   : testDEIMconvergence
Date   : Feb 19 2025
Time   : 17:52:08
Host   : REN-1466-1-P568
PID    : 27051
I/O    : uncollated
Case   : /media/vresseguier/55222cef-2cd1-457f-83ca-21f4b445bbbb/red_lum_data/RedLUM/data_red_lum_cpp/LESRe100/ROM_ref/DEIM/Hyp_1_Sto_1_adv_1_Int_1_DEIMInt_fullStressFunction_inf_1_pod_H1
nProcs : 1
trapFpe: Floating point exception trapping enabled (FOAM_SIGFPE).
fileModificationChecking : Monitoring run-time modified files using timeStampMaster (fileModificationSkew 5, maxFileModificationPolls 20)
allowSystemOperations : Allowing user-supplied system call operations

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
argv[0] = testDEIMconvergence
---------------------------------------------------------
Simulation type:"LES"
LESmodel:"Smagorinsky"
Simulation type LES Smagorinsky, DEIM will be performed on non linear term
DEIM will be perfomed on fullStressFunction terms.
 Stochastic Hyperreduction  will be perfomed
---------------------------------------------------------
RedLUM simulate (ver:9a6ca9b0) START
Ithaca-FV POD decompose U fields
-------------------------------------------------------------------------------------------
The POD is performing with the following parameters :
Field : U
Number of modes : 4
POD Hilbert space : H1
Weight for the boundary conditions (0 for L2 POD Hilbert space) : 0
Patch for the boundary conditions(inlet by default) used in L2wBC : inlet
start time : 2
End time : 32
Number of snapshots : 31
Number of test snapshots : 31
Number of blocks : 2
Centered datas or not : 1 (1 centered, 0 not centered)
Name of eigensolver used : spectra
Results folder : ITHACAoutput
Computing the mean of U field
######### Exporting the Data for U #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Computing the covariance matrix of the U field
q = 15
r = 1
nSnapshot = 31
nBlocks = 2

######### Reading the Data for U #########
 17% [||||||||||                                                  ] 23% [||||||||||||||                                              ] 29% [|||||||||||||||||                                           ] 35% [|||||||||||||||||||||                                       ] 41% [||||||||||||||||||||||||                                    ] 47% [||||||||||||||||||||||||||||                                ] 52% [|||||||||||||||||||||||||||||||                             ] 58% [|||||||||||||||||||||||||||||||||||                         ] 64% [||||||||||||||||||||||||||||||||||||||                      ] 70% [||||||||||||||||||||||||||||||||||||||||||                  ] 76% [|||||||||||||||||||||||||||||||||||||||||||||               ] 82% [|||||||||||||||||||||||||||||||||||||||||||||||||           ] 88% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 94% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the triangular block [0:14]x[0:14] to the covariance matrix

######### Reading the Data for U #########
 56% [|||||||||||||||||||||||||||||||||                           ] 59% [|||||||||||||||||||||||||||||||||||                         ] 62% [|||||||||||||||||||||||||||||||||||||                       ] 65% [|||||||||||||||||||||||||||||||||||||||                     ] 68% [|||||||||||||||||||||||||||||||||||||||||                   ] 71% [|||||||||||||||||||||||||||||||||||||||||||                 ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ] 78% [||||||||||||||||||||||||||||||||||||||||||||||              ] 81% [||||||||||||||||||||||||||||||||||||||||||||||||            ] 84% [||||||||||||||||||||||||||||||||||||||||||||||||||          ] 87% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 90% [||||||||||||||||||||||||||||||||||||||||||||||||||||||      ] 93% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ] 96% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||  ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the triangular block [15:29]x[15:29] to the covariance matrix

######### Reading the Data for U #########
 17% [||||||||||                                                  ] 23% [||||||||||||||                                              ] 29% [|||||||||||||||||                                           ] 35% [|||||||||||||||||||||                                       ] 41% [||||||||||||||||||||||||                                    ] 47% [||||||||||||||||||||||||||||                                ] 52% [|||||||||||||||||||||||||||||||                             ] 58% [|||||||||||||||||||||||||||||||||||                         ] 64% [||||||||||||||||||||||||||||||||||||||                      ] 70% [||||||||||||||||||||||||||||||||||||||||||                  ] 76% [|||||||||||||||||||||||||||||||||||||||||||||               ] 82% [|||||||||||||||||||||||||||||||||||||||||||||||||           ] 88% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 94% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the square block [15:29]x[0:14] to the covariance matrix

######### Reading the Data for U #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the triangular block [30:30]x[30:30] to the covariance matrix

######### Reading the Data for U #########
 17% [||||||||||                                                  ] 23% [||||||||||||||                                              ] 29% [|||||||||||||||||                                           ] 35% [|||||||||||||||||||||                                       ] 41% [||||||||||||||||||||||||                                    ] 47% [||||||||||||||||||||||||||||                                ] 52% [|||||||||||||||||||||||||||||||                             ] 58% [|||||||||||||||||||||||||||||||||||                         ] 64% [||||||||||||||||||||||||||||||||||||||                      ] 70% [||||||||||||||||||||||||||||||||||||||||||                  ] 76% [|||||||||||||||||||||||||||||||||||||||||||||               ] 82% [|||||||||||||||||||||||||||||||||||||||||||||||||           ] 88% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 94% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the square block [30:30]x[0:14] to the covariance matrix

######### Reading the Data for U #########
 56% [|||||||||||||||||||||||||||||||||                           ] 59% [|||||||||||||||||||||||||||||||||||                         ] 62% [|||||||||||||||||||||||||||||||||||||                       ] 65% [|||||||||||||||||||||||||||||||||||||||                     ] 68% [|||||||||||||||||||||||||||||||||||||||||                   ] 71% [|||||||||||||||||||||||||||||||||||||||||||                 ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ] 78% [||||||||||||||||||||||||||||||||||||||||||||||              ] 81% [||||||||||||||||||||||||||||||||||||||||||||||||            ] 84% [||||||||||||||||||||||||||||||||||||||||||||||||||          ] 87% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 90% [||||||||||||||||||||||||||||||||||||||||||||||||||||||      ] 93% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ] 96% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||  ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the square block [30:30]x[15:29] to the covariance matrix

Total varying L2 energy for U : 0.18077632

Computing the mean of gradU field
######### Exporting the Data for gradU #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Computing the covariance matrix of the gradU field
q = 15
r = 1
nSnapshot = 31
nBlocks = 2

######### Reading the Data for gradU #########
 17% [||||||||||                                                  ] 23% [||||||||||||||                                              ] 29% [|||||||||||||||||                                           ] 35% [|||||||||||||||||||||                                       ] 41% [||||||||||||||||||||||||                                    ] 47% [||||||||||||||||||||||||||||                                ] 52% [|||||||||||||||||||||||||||||||                             ] 58% [|||||||||||||||||||||||||||||||||||                         ] 64% [||||||||||||||||||||||||||||||||||||||                      ] 70% [||||||||||||||||||||||||||||||||||||||||||                  ] 76% [|||||||||||||||||||||||||||||||||||||||||||||               ] 82% [|||||||||||||||||||||||||||||||||||||||||||||||||           ] 88% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 94% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the triangular block [0:14]x[0:14] to the covariance matrix

######### Reading the Data for gradU #########
 56% [|||||||||||||||||||||||||||||||||                           ] 59% [|||||||||||||||||||||||||||||||||||                         ] 62% [|||||||||||||||||||||||||||||||||||||                       ] 65% [|||||||||||||||||||||||||||||||||||||||                     ] 68% [|||||||||||||||||||||||||||||||||||||||||                   ] 71% [|||||||||||||||||||||||||||||||||||||||||||                 ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ] 78% [||||||||||||||||||||||||||||||||||||||||||||||              ] 81% [||||||||||||||||||||||||||||||||||||||||||||||||            ] 84% [||||||||||||||||||||||||||||||||||||||||||||||||||          ] 87% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 90% [||||||||||||||||||||||||||||||||||||||||||||||||||||||      ] 93% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ] 96% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||  ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the triangular block [15:29]x[15:29] to the covariance matrix

######### Reading the Data for gradU #########
 17% [||||||||||                                                  ] 23% [||||||||||||||                                              ] 29% [|||||||||||||||||                                           ] 35% [|||||||||||||||||||||                                       ] 41% [||||||||||||||||||||||||                                    ] 47% [||||||||||||||||||||||||||||                                ] 52% [|||||||||||||||||||||||||||||||                             ] 58% [|||||||||||||||||||||||||||||||||||                         ] 64% [||||||||||||||||||||||||||||||||||||||                      ] 70% [||||||||||||||||||||||||||||||||||||||||||                  ] 76% [|||||||||||||||||||||||||||||||||||||||||||||               ] 82% [|||||||||||||||||||||||||||||||||||||||||||||||||           ] 88% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 94% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the square block [15:29]x[0:14] to the covariance matrix

######### Reading the Data for gradU #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the triangular block [30:30]x[30:30] to the covariance matrix

######### Reading the Data for gradU #########
 17% [||||||||||                                                  ] 23% [||||||||||||||                                              ] 29% [|||||||||||||||||                                           ] 35% [|||||||||||||||||||||                                       ] 41% [||||||||||||||||||||||||                                    ] 47% [||||||||||||||||||||||||||||                                ] 52% [|||||||||||||||||||||||||||||||                             ] 58% [|||||||||||||||||||||||||||||||||||                         ] 64% [||||||||||||||||||||||||||||||||||||||                      ] 70% [||||||||||||||||||||||||||||||||||||||||||                  ] 76% [|||||||||||||||||||||||||||||||||||||||||||||               ] 82% [|||||||||||||||||||||||||||||||||||||||||||||||||           ] 88% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 94% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the square block [30:30]x[0:14] to the covariance matrix

######### Reading the Data for gradU #########
 56% [|||||||||||||||||||||||||||||||||                           ] 59% [|||||||||||||||||||||||||||||||||||                         ] 62% [|||||||||||||||||||||||||||||||||||||                       ] 65% [|||||||||||||||||||||||||||||||||||||||                     ] 68% [|||||||||||||||||||||||||||||||||||||||||                   ] 71% [|||||||||||||||||||||||||||||||||||||||||||                 ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ] 78% [||||||||||||||||||||||||||||||||||||||||||||||              ] 81% [||||||||||||||||||||||||||||||||||||||||||||||||            ] 84% [||||||||||||||||||||||||||||||||||||||||||||||||||          ] 87% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 90% [||||||||||||||||||||||||||||||||||||||||||||||||||||||      ] 93% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ] 96% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||  ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the square block [30:30]x[15:29] to the covariance matrix

Total varying L2 energy for gradU : 0.43496303

Total varying energy H1 : 0.61573935

Performing the eigen decomposition
Using Spectra EigenSolver 
% of varying H1 energy captures by the modes for U : 98.750218%

Computing the spatial modes of the U field
######### Exporting the Data for U #########
 25% [|||||||||||||||                                             ] 50% [||||||||||||||||||||||||||||||                              ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Computing the temporal modes of the U field

Computing the Simulation temporal modes of the U field

-------------------------------------------------------------------------------------------
Computing the covariance matrix of resolved speed

Computing the covariance matrix of unresolved speed

Computing cov_s vector

Computing deriv_cov_s vector

interval_subsample = 1
l_nSnapshot = 31
time_interval_subsample = 1

Computing variance tensor a
######### Reading the Data for U #########
 50% [||||||||||||||||||||||||||||||                              ] 66% [||||||||||||||||||||||||||||||||||||||||                    ] 83% [||||||||||||||||||||||||||||||||||||||||||||||||||          ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Reading the Data for U #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
nSubsample=31
DEIM on fullStressFunction
TODO : create template_field_Smag in parameter
-------------------------------------------------------------------------------------------
The POD is performing with the following parameters :
Field : fullStressFunction
Number of modes : 4
POD Hilbert space : L2
Weight for the boundary conditions (0 for L2 POD Hilbert space) : 0
Patch for the boundary conditions(inlet by default) used in L2wBC : inlet
start time : 2
End time : 32
Number of snapshots : 31
Number of test snapshots : 31
Number of blocks : 2
Centered datas or not : 1 (1 centered, 0 not centered)
Name of eigensolver used : spectra
Results folder : ITHACAoutput
Computing the mean of fullStressFunction field
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Computing the covariance matrix of the fullStressFunction field
q = 15
r = 1
nSnapshot = 31
nBlocks = 2

######### Reading the Data for fullStressFunction #########
 17% [||||||||||                                                  ] 23% [||||||||||||||                                              ] 29% [|||||||||||||||||                                           ] 35% [|||||||||||||||||||||                                       ] 41% [||||||||||||||||||||||||                                    ] 47% [||||||||||||||||||||||||||||                                ] 52% [|||||||||||||||||||||||||||||||                             ] 58% [|||||||||||||||||||||||||||||||||||                         ] 64% [||||||||||||||||||||||||||||||||||||||                      ] 70% [||||||||||||||||||||||||||||||||||||||||||                  ] 76% [|||||||||||||||||||||||||||||||||||||||||||||               ] 82% [|||||||||||||||||||||||||||||||||||||||||||||||||           ] 88% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 94% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the triangular block [0:14]x[0:14] to the covariance matrix

######### Reading the Data for fullStressFunction #########
 56% [|||||||||||||||||||||||||||||||||                           ] 59% [|||||||||||||||||||||||||||||||||||                         ] 62% [|||||||||||||||||||||||||||||||||||||                       ] 65% [|||||||||||||||||||||||||||||||||||||||                     ] 68% [|||||||||||||||||||||||||||||||||||||||||                   ] 71% [|||||||||||||||||||||||||||||||||||||||||||                 ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ] 78% [||||||||||||||||||||||||||||||||||||||||||||||              ] 81% [||||||||||||||||||||||||||||||||||||||||||||||||            ] 84% [||||||||||||||||||||||||||||||||||||||||||||||||||          ] 87% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 90% [||||||||||||||||||||||||||||||||||||||||||||||||||||||      ] 93% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ] 96% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||  ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the triangular block [15:29]x[15:29] to the covariance matrix

######### Reading the Data for fullStressFunction #########
 17% [||||||||||                                                  ] 23% [||||||||||||||                                              ] 29% [|||||||||||||||||                                           ] 35% [|||||||||||||||||||||                                       ] 41% [||||||||||||||||||||||||                                    ] 47% [||||||||||||||||||||||||||||                                ] 52% [|||||||||||||||||||||||||||||||                             ] 58% [|||||||||||||||||||||||||||||||||||                         ] 64% [||||||||||||||||||||||||||||||||||||||                      ] 70% [||||||||||||||||||||||||||||||||||||||||||                  ] 76% [|||||||||||||||||||||||||||||||||||||||||||||               ] 82% [|||||||||||||||||||||||||||||||||||||||||||||||||           ] 88% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 94% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the square block [15:29]x[0:14] to the covariance matrix

######### Reading the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the triangular block [30:30]x[30:30] to the covariance matrix

######### Reading the Data for fullStressFunction #########
 17% [||||||||||                                                  ] 23% [||||||||||||||                                              ] 29% [|||||||||||||||||                                           ] 35% [|||||||||||||||||||||                                       ] 41% [||||||||||||||||||||||||                                    ] 47% [||||||||||||||||||||||||||||                                ] 52% [|||||||||||||||||||||||||||||||                             ] 58% [|||||||||||||||||||||||||||||||||||                         ] 64% [||||||||||||||||||||||||||||||||||||||                      ] 70% [||||||||||||||||||||||||||||||||||||||||||                  ] 76% [|||||||||||||||||||||||||||||||||||||||||||||               ] 82% [|||||||||||||||||||||||||||||||||||||||||||||||||           ] 88% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 94% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the square block [30:30]x[0:14] to the covariance matrix

######### Reading the Data for fullStressFunction #########
 56% [|||||||||||||||||||||||||||||||||                           ] 59% [|||||||||||||||||||||||||||||||||||                         ] 62% [|||||||||||||||||||||||||||||||||||||                       ] 65% [|||||||||||||||||||||||||||||||||||||||                     ] 68% [|||||||||||||||||||||||||||||||||||||||||                   ] 71% [|||||||||||||||||||||||||||||||||||||||||||                 ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ] 78% [||||||||||||||||||||||||||||||||||||||||||||||              ] 81% [||||||||||||||||||||||||||||||||||||||||||||||||            ] 84% [||||||||||||||||||||||||||||||||||||||||||||||||||          ] 87% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 90% [||||||||||||||||||||||||||||||||||||||||||||||||||||||      ] 93% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ] 96% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||  ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the square block [30:30]x[15:29] to the covariance matrix

Total varying L2 energy for fullStressFunction : 2.4463809e-07

Performing the eigen decomposition
Using Spectra EigenSolver 
% of varying L2 energy captures by the modes for fullStressFunction : 90.007051%

Computing the spatial modes of the fullStressFunction field
######### Exporting the Data for fullStressFunction #########
 25% [|||||||||||||||                                             ] 50% [||||||||||||||||||||||||||||||                              ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Computing the temporal modes of the fullStressFunction field

Computing the Simulation temporal modes of the fullStressFunction field

-------------------------------------------------------------------------------------------
This is the new VERSION with ITHACA HyperReduction
Init HyperReduction class with vectorial dim: 3
Initial seeds length: 0
FolderMethod : ./ITHACAoutput/DEIM_centered/fullStressFunction/4magicPoints/
####### Begin Greedy-L2 GappyDEIM #######
####### Modes=4, nodePoints=4 #######
####### End of greedy GappyDEIM #######
S-Optimalty: 0.97522516
Inversion method : completeOrthogonalDecomposition
Projection Matrix shape: 34932 12
Basis Matrix shape: 34932 4
Pseudo Inverse Matrix shape: 4 12
Condition number of P^T U = 1.9764084
####### Extract submesh #######
####### End extract submesh size = 43 #######
####### End create masks #######
Compute time average of ||grad(v')||² at magic points.
######### Exporting the Data for gradU #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for gradU #########
 25% [|||||||||||||||                                             ] 50% [||||||||||||||||||||||||||||||                              ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
we consider varianceTensorParameters of velocity 
Computing variance tensor a
######### Reading the Data for gradU #########
 50% [||||||||||||||||||||||||||||||                              ] 66% [||||||||||||||||||||||||||||||||||||||||                    ] 83% [||||||||||||||||||||||||||||||||||||||||||||||||||          ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Reading the Data for gradU #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
nSubsample=31
-------------------------------------------------------------------------------------------
IthavaFV Reducer

Mass matrix
 PseudoInverse Mass matrix
Condition number of M_matrix = 3.2380929
######### Reading the Data for fullStressFunction #########
 50% [||||||||||||||||||||||||||||||                              ] 66% [||||||||||||||||||||||||||||||||||||||||                    ] 83% [||||||||||||||||||||||||||||||||||||||||||||||||||          ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.0049495238, No Iterations 5

Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.0058204872, No Iterations 5

Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.0080761954, No Iterations 4

Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.0098169835, No Iterations 4
######### Exporting the Data for fullStressFunction_PFD #########
 25% [|||||||||||||||                                             ] 50% [||||||||||||||||||||||||||||||                              ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.0031129574, No Iterations 4
Mass matrix
 PseudoInverse Mass matrix
Condition number of M_matrix = 3.2380929
Resolving a time step of IthacaFV Resolver
IthacaFVErrorDEIM : Reading the spatial modes of the fullStressFunction field
######### Reading the Data for fullStressFunction #########
 50% [||||||||||||||||||||||||||||||                              ] 66% [||||||||||||||||||||||||||||||||||||||||                    ] 83% [||||||||||||||||||||||||||||||||||||||||||||||||||          ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
IthacaFVErrorDEIM : Reading the spatial modes of the U field
######### Reading the Data for U #########
 50% [||||||||||||||||||||||||||||||                              ] 66% [||||||||||||||||||||||||||||||||||||||||                    ] 83% [||||||||||||||||||||||||||||||||||||||||||||||||||          ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Reading the mean of U field
######### Reading the Data for U #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
 Temporal Mode U 0 = 0.75076643
 Temporal Mode U 1 = 0.033809868
 Temporal Mode U 2 = -0.1586954
 Temporal Mode U 3 = -0.078844079
Cell volume 9.347117e-07
Cell volume 0.002125
Cell volume 0.002125
Cell volume 0.002125
######### Exporting the Data for magicPointsField #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
m = 4 magic pts // n = 4 modes // 
 --------------------------------------------------------
Generate_files_report atMgPts
 --------------------------------------------------------
Relative error at magic pts number 0 =0.00692458
Relative error at magic pts number 1 =0.079571389
Relative error at magic pts number 2 =0.29367329
Relative error at magic pts number 3 =0.18563411
 --------------------------------------------------------

 --------------------------------------------------------
Generate_files_report fromOpenFOAM
 --------------------------------------------------------
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for proj_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Metrics compared fromOpenFOAM

 Unprojected field 
|| f - ^f||_L2 / || f ||_L2 = 0.032142465
E_*(f) = 0.031475615
C = || (P^T U)^-1|| = 7.6622159
C*E_*(f) = 0.24117296
bound on C = 167458.99

######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for proj_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
 Projected field 
|| Pi(f) - Pi(^f)||_L2 / || Pi(f) ||_L2 = 0.066082903
|| Pi( (Id - U U^T )(f) )||_L2 / || Pi(f) ||_L2  = 0.032606217


Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.0031129574, No Iterations 4
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.0030970905, No Iterations 4
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for proj_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.0036750895, No Iterations 4
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.0030970905, No Iterations 4
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
 Projected field with Leray proj L (PFD)
|| Pi(L(f)) - Pi(^L(f))||_L2 / || Pi(L(f)) ||_L2 = 0.066380125
|| Pi( (Id - U U^T )(L(f)) )||_L2 / || Pi(L(f)) ||_L2  = 0.03220323

 --------------------------------------------------------

 --------------------------------------------------------
Generate_files_report fromRedU
 --------------------------------------------------------
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for proj_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Metrics compared fromRedU

 Unprojected field 
|| f - ^f||_L2 / || f ||_L2 = 0.01748022
E_*(f) = 0.017066991
C = || (P^T U)^-1|| = 7.6622159
C*E_*(f) = 0.13077097
bound on C = 167458.99

######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for proj_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
 Projected field 
|| Pi(f) - Pi(^f)||_L2 / || Pi(f) ||_L2 = 0.037050363
|| Pi( (Id - U U^T )(f) )||_L2 / || Pi(f) ||_L2  = 0.017822441


Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.0031129574, No Iterations 4
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.0031281148, No Iterations 4
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for fullStressFunction #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
######### Exporting the Data for proj_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.006256583, No Iterations 4
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Performing projection onto the space of divergence-free functions
GAMG:  Solving for p, Initial residual = 1, Final residual = 0.0031281148, No Iterations 4
######### Exporting the Data for projV_f #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
 Projected field with Leray proj L (PFD)
|| Pi(L(f)) - Pi(^L(f))||_L2 / || Pi(L(f)) ||_L2 = 0.0369302
|| Pi( (Id - U U^T )(L(f)) )||_L2 / || Pi(L(f)) ||_L2  = 0.017970353

 --------------------------------------------------------

Ignoring IthacaFV Synthetizer
RedLUM simulate (ver:9a6ca9b0) END
