/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.2.2                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      ITHACAdict;
}

fields
(
U_pod
p_pod
nut_pod
fullStressFunction_pod
);

U_pod
{
// Specify the field name
field_name U;
// Specify the number of modes you want to extract
nmodes 4;
// Specify the type of field (if vector or scalar)
field_type vector;

hilbertSpacePOD H1;
}

p_pod
{
// Specify the field name
field_name p;
// Specify the number of modes you want to extract
nmodes 4;
// Specify the type of field (if vector or scalar)
field_type scalar;

hilbertSpacePOD L2; //skipthisline
}

// For nut and fullStressFunction
// the number of modes is also the number of magicPoints 
// for the DEIM
nut_pod
{
// Specify the field name
field_name nut;
// Specify the number of modes you want to extract
field_type scalar;
// Specify the type of field (if vector or scalar)
nmodes 4; //nut
// Specify the hilbert space
hilbertSpacePOD L2; //skipthisline
}

fullStressFunction_pod
{
// Specify the field name
field_name fullStressFunction;
// Specify the number of modes you want to extract
field_type scalar;
// Specify the type of field (if vector or scalar)
nmodes 4; //fullStressFunction
// Specify the hilbert space
hilbertSpacePOD L2; //skipthisline
}

// Root path where OpenFOAM simulation outputs are
rootpath ".";

// Path (from rootPath) where OpenFOAM simulation outputs are
casename "../../../openfoam_data/";

//// Path where OpenFOAM simulation outputs are
//pathData "../sillageRe300/";

// Set Time from which you want to start taking the Snapshots
InitialTime 500;

// Set Time when you want to stop taking the Snapshots
FinalTime 530;

// Set Time when you want to stop taking the Snapshots
FinalTimeSimulation 560;

// Eventually you could set just InitalTime and Number of Snapshots you want
//Nsnapshots 40;

// number of blocks to compute cov matrix
nBlocks 2;

// if 1, the snapshots are centered before POD (snapshots = snapshots - meanField)
centeredOrNot 1;

// if 1, the interpolated field snapshots are centered before POD (snapshots = snapshots - meanField)
interpFieldCenteredOrNot 1;

// 1 if you want to export reduce matrices on specific format
exportPython 0;
exportMatlab 0;
exportTxt 1;

// Output format to save market vectors.
OutPrecision 20;
OutType fixed;

// name of eigensolver : eigen or spectra
// EigenSolver eigen;

// name of results folder : ITHACAoutput by default
// resultsFolder ITHACAoutput;

// Number of stochastic POD simulations
nParticules 2;

//reducingNoisesMatrix 1;

// Time step decreasing factor for ROM time integration
// N_resolve 1;
//nSimu 100;


// Choice between nut or fullStressFunction for the DEIM
DEIMInterpolatedField fullStressFunction;

//onLineReconstruct 1;

// HyperReduction Class from ITHACA
// HR
HyperReduction GappyDEIM;//GappyDEIM, ECP
GreedyMetric L2;//L2, SOPT, SOPTE

// InversionMethod
// Possibilities : pinv_eigen_based, half_pinv_eigen_based, direct, fullPivLu,
// partialPivLu, householderQr, colPivHouseholderQr, fullPivHouseholderQr,
// completeOrthogonalDecomposition, jacobiSvd, llt, ldlt, bdcSvd
InversionMethod completeOrthogonalDecomposition;


// Activate stochastic
stochasOrNot 1;

// Hyper Reduction Stochastic
HypRedSto 1;

// Inflate NuT
inflatNut 1;

advModifOrNot 1;
