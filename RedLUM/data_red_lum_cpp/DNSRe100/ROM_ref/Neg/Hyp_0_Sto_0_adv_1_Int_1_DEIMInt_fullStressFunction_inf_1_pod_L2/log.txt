/*---------------------------------------------------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2212                                  |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
Build  : _70874860-20230612 OPENFOAM=2212 patch=230612 version=2212
Arch   : "LSB;label=32;scalar=64"
Exec   : redlum-neglectedPressure
Date   : Feb 19 2025
Time   : 17:51:42
Host   : REN-1466-1-P568
PID    : 26591
I/O    : uncollated
Case   : /media/vresseguier/55222cef-2cd1-457f-83ca-21f4b445bbbb/red_lum_data/RedLUM/data_red_lum_cpp/DNSRe100/ROM_ref/Neg/Hyp_0_Sto_0_adv_1_Int_1_DEIMInt_fullStressFunction_inf_1_pod_L2
nProcs : 1
trapFpe: Floating point exception trapping enabled (FOAM_SIGFPE).
fileModificationChecking : Monitoring run-time modified files using timeStampMaster (fileModificationSkew 5, maxFileModificationPolls 20)
allowSystemOperations : Allowing user-supplied system call operations

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
argv[0] = redlum-neglectedPressure
---------------------------------------------------------
Simulation type:"laminar"
DNS simulation will be performed
---------------------------------------------------------
RedLUM simulate (ver:9a6ca9b0) START
Ithaca-FV POD decompose U field
-------------------------------------------------------------------------------------------
The POD is performing with the following parameters :
Field : U
Number of modes : 4
POD Hilbert space : L2
Weight for the boundary conditions (0 for L2 POD Hilbert space) : 0
Patch for the boundary conditions(inlet by default) used in L2wBC : inlet
start time : 2
End time : 32
Number of snapshots : 31
Number of test snapshots : 31
Number of blocks : 2
Centered datas or not : 1 (1 centered, 0 not centered)
Name of eigensolver used : spectra
Results folder : ITHACAoutput
Computing the mean of U field
######### Exporting the Data for U #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Computing the covariance matrix of the U field
q = 15
r = 1
nSnapshot = 31
nBlocks = 2

######### Reading the Data for U #########
 17% [||||||||||                                                  ] 23% [||||||||||||||                                              ] 29% [|||||||||||||||||                                           ] 35% [|||||||||||||||||||||                                       ] 41% [||||||||||||||||||||||||                                    ] 47% [||||||||||||||||||||||||||||                                ] 52% [|||||||||||||||||||||||||||||||                             ] 58% [|||||||||||||||||||||||||||||||||||                         ] 64% [||||||||||||||||||||||||||||||||||||||                      ] 70% [||||||||||||||||||||||||||||||||||||||||||                  ] 76% [|||||||||||||||||||||||||||||||||||||||||||||               ] 82% [|||||||||||||||||||||||||||||||||||||||||||||||||           ] 88% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 94% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the triangular block [0:14]x[0:14] to the covariance matrix

######### Reading the Data for U #########
 56% [|||||||||||||||||||||||||||||||||                           ] 59% [|||||||||||||||||||||||||||||||||||                         ] 62% [|||||||||||||||||||||||||||||||||||||                       ] 65% [|||||||||||||||||||||||||||||||||||||||                     ] 68% [|||||||||||||||||||||||||||||||||||||||||                   ] 71% [|||||||||||||||||||||||||||||||||||||||||||                 ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ] 78% [||||||||||||||||||||||||||||||||||||||||||||||              ] 81% [||||||||||||||||||||||||||||||||||||||||||||||||            ] 84% [||||||||||||||||||||||||||||||||||||||||||||||||||          ] 87% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 90% [||||||||||||||||||||||||||||||||||||||||||||||||||||||      ] 93% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ] 96% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||  ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the triangular block [15:29]x[15:29] to the covariance matrix

######### Reading the Data for U #########
 17% [||||||||||                                                  ] 23% [||||||||||||||                                              ] 29% [|||||||||||||||||                                           ] 35% [|||||||||||||||||||||                                       ] 41% [||||||||||||||||||||||||                                    ] 47% [||||||||||||||||||||||||||||                                ] 52% [|||||||||||||||||||||||||||||||                             ] 58% [|||||||||||||||||||||||||||||||||||                         ] 64% [||||||||||||||||||||||||||||||||||||||                      ] 70% [||||||||||||||||||||||||||||||||||||||||||                  ] 76% [|||||||||||||||||||||||||||||||||||||||||||||               ] 82% [|||||||||||||||||||||||||||||||||||||||||||||||||           ] 88% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 94% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the square block [15:29]x[0:14] to the covariance matrix

######### Reading the Data for U #########
100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the triangular block [30:30]x[30:30] to the covariance matrix

######### Reading the Data for U #########
 17% [||||||||||                                                  ] 23% [||||||||||||||                                              ] 29% [|||||||||||||||||                                           ] 35% [|||||||||||||||||||||                                       ] 41% [||||||||||||||||||||||||                                    ] 47% [||||||||||||||||||||||||||||                                ] 52% [|||||||||||||||||||||||||||||||                             ] 58% [|||||||||||||||||||||||||||||||||||                         ] 64% [||||||||||||||||||||||||||||||||||||||                      ] 70% [||||||||||||||||||||||||||||||||||||||||||                  ] 76% [|||||||||||||||||||||||||||||||||||||||||||||               ] 82% [|||||||||||||||||||||||||||||||||||||||||||||||||           ] 88% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 94% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the square block [30:30]x[0:14] to the covariance matrix

######### Reading the Data for U #########
 56% [|||||||||||||||||||||||||||||||||                           ] 59% [|||||||||||||||||||||||||||||||||||                         ] 62% [|||||||||||||||||||||||||||||||||||||                       ] 65% [|||||||||||||||||||||||||||||||||||||||                     ] 68% [|||||||||||||||||||||||||||||||||||||||||                   ] 71% [|||||||||||||||||||||||||||||||||||||||||||                 ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ] 78% [||||||||||||||||||||||||||||||||||||||||||||||              ] 81% [||||||||||||||||||||||||||||||||||||||||||||||||            ] 84% [||||||||||||||||||||||||||||||||||||||||||||||||||          ] 87% [||||||||||||||||||||||||||||||||||||||||||||||||||||        ] 90% [||||||||||||||||||||||||||||||||||||||||||||||||||||||      ] 93% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||    ] 96% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||  ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]
Adding the square block [30:30]x[15:29] to the covariance matrix

Total varying L2 energy for U : 0.188171

Performing the eigen decomposition
Using Spectra EigenSolver 
% of varying L2 energy captures by the modes for U : 99.7572%

Computing the spatial modes of the U field
######### Exporting the Data for U #########
 25% [|||||||||||||||                                             ] 50% [||||||||||||||||||||||||||||||                              ] 75% [|||||||||||||||||||||||||||||||||||||||||||||               ]100% [||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||]

Computing the temporal modes of the U field

Computing the Simulation temporal modes of the U field

-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
IthavaFV Neglected pressure Reducer

Adding the mean as a lifting function for the speed
5
Diffusion term

Advection term

Mass Induce Terms
Mass matrix
 PseudoInverse Mass matrix
Condition number of M_matrix = 1
Diffusion term with PseudoInverse Mass matrix
Advection term with PseudoInverse Mass matrix
-------------------------------------------------------------------------------------------
IthacaFV Resolver Neglected pressure
2 realizations of the resolution have been launched


 --- Resolution without Assimilation ---

 --- Resolution without saving Brownians ---

     NOTE : run takes 1e-05 seconds before resolution (getting parameters)


     NOTE : run takes 0.015324 seconds for resolution

Cf. [python files] in ./ITHACAoutput/Reduced_coeff_4_0.01_2_neglectedPressure_centered/ :
   from approx_temporalModes_U_0.npy to approx_temporalModes_U_1.npy
-------------------------------------------------------------------------------------------
IthacaFV Synthetizer Mean
-------------------------------------------------------------------------------------------
IthacaFV Synthetizer Error
---------------------------------------END------------------------------------------------
RedLUM simulate (ver:9a6ca9b0) END
