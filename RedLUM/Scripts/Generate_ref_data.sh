echo "=================Welcome to Redlum Data================="
echo ""

# Check if an argument is provided
if [ $# -eq 0 ];
  then    echo "Error: No input argument provided."
  echo "You must provide a tag for the commit"
  echo "Usage: $0 <argument>"
  exit 1
fi

# Clean IthacaOutput directories
bash RemoveIthacaOutputs.sh ref

# Generate ref cases
bash test_RedLUM.sh ref

# Commit all modified files, this may take some times
git add -A

# Commit
git commit -m "Generating data for tag $1"

# Set the tag of the commit
echo "Setting tag to $1"
git tag $1

# Push results
git push origin --tags

exit 0

