#!/bin/bash


# Function to check if a string is in a dictionnary
function check_string_in_directories() {
  local search_string="$1"
  local search_directory="$2"

  # Loop through all directories in the search directory
  for dir in "$search_directory"/*/; do
    # Check if the directory name contains the search string
    if [[ "$(basename "$dir")" == *"$search_string"* ]]; then
      echo "Directory '$dir' contains the string '$search_string'."
    else
      echo "Directory '$dir' does not contain the string '$search_string'."
    fi
  done
}
# Changing the time format os that the time command shows only the real time
TIMEFORMAT=%R

echo "=================Welcome to Redlum Cleaning Script================="
echo ""
if [[ $1 == "ref" ]]; then
  echo "Cleaning reference cases"
  echo ""
else
  echo "Cleaning test cases"
  echo ""
fi



cd "../data_red_lum_cpp" || exit
for dtype in */; do

    echo "Now considering " "${dtype#/}" "data"
    echo "=================================="

    if [[ $1 == "ref" ]]; then
      ROM_dir="${dtype#/}/ROM_ref"
    else
      ROM_dir="${dtype#/}/ROM_test"
    fi

    cd $ROM_dir

    for exe_case in */; do
      cd "$exe_case"
        for param_case in */; do
          echo "Removing ITHACAOutput of:" $exe_case "for" $param_case
          cd "$param_case"
          
       	    touch ITHACAoutput
            rm -R ITHACAoutput

         
          cd ..
          echo ""
        done
      cd ..
    done
    cd ../..
done
