##
from glob import glob
import numpy as np
import pandas as pd
import utils
from Generate_all_case import remove_exceptions


dtype_list = ["DNSRe100","LESRe100"]
rtype_list = ["Neg", "Red", "Full", "DEIM", "DEIMNut", "DEIMSmag"]

df_res = pd.DataFrame(index=rtype_list)
for dtype in dtype_list:
    df_res[dtype] = 0

print(df_res)


list_setup = np.load("npy/list_setup_cleaned.npy",allow_pickle=True)

##
dict_res = dict({})
for dtype in dtype_list:
    dict_res[dtype] = dict({})

for dtype in dtype_list:
    for rtype in rtype_list:
        dict_res[dtype][rtype] = dict({})

##
dtype = "DNSRe100"
rtype = "Neg"

for setup in list_setup:
    name_dir_setup = utils.setup_to_dirname(setup)

    dtype = setup["dtype"]
    rtype = setup["rtype"]


    path_log = f"../../data_red_lum_cpp/{dtype}/ROM_ref/{rtype}/{name_dir_setup}"
    with open(f"{path_log}/log.txt","r") as f:
        lines = f.readlines()


    if "END" in lines[-1]: # == "---------------------------------------END------------------------------------------------":
        # Run was a success
        dict_res[dtype][rtype][name_dir_setup] = 1

    elif remove_exceptions([setup]) == []:
        dict_res[dtype][rtype][name_dir_setup] = 0


    else:
        # There was something wrong with the run
        dict_res[dtype][rtype][name_dir_setup] = -1

    print(dict_res[dtype][rtype][name_dir_setup])


##
# for dtype in dtype_list:
#     for rtype in rtype_list:

##