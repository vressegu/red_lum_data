import json

import numpy as np
import os
import shutil
import fileinput
import itertools

def get_list_param():
    """

    :return: List of parameter for a given setup
    """

    return ["Hyp","Sto","adv","Int","DEIMInt","inf","pod"]

def setup_to_dirname(dict_setup):
    """
    :return: Get the name of the directory for a given setup
    """

    Hyp = dict_setup["HypRedSto"]
    Sto = dict_setup["stochasOrNot"]
    adv = dict_setup["advModifOrNot"]
    Int = dict_setup["interpFieldCenteredOrNot"]
    inf = dict_setup["inflatNut"]
    DEIMInt = dict_setup["DEIMInterpolatedField"]
    pod = dict_setup["hilbertSpacePOD"]

    # TODO do a loop over a list of parameters
    dirname = f"Hyp_{Hyp}_Sto_{Sto}_adv_{adv}_Int_{Int}_DEIMInt_{DEIMInt}_inf_{inf}_pod_{pod}"

    if "useStratonovich" in dict_setup.keys():
        strat = dict_setup["useStratonovich"]
        dirname += f"_strato_{strat}"

    return dirname

def product_dict(**kwargs):
    """
    :param kwargs:
    :return: list of all the parameter
    """
    keys = kwargs.keys()
    for instance in itertools.product(*kwargs.values()):
        yield dict(zip(keys, instance))

def setup_dir(dtype,path_case):
    """
    Copy and paste the required data from the openfoam directory.

    :param dtype: simulation type (DNS, LES)
    :param path_case: test case directory
    :return:
    """
    os.makedirs(path_case, exist_ok=True)
    shutil.copytree(f"RedLUM/data_red_lum_cpp/{dtype}/openfoam_data/0",f"{path_case}/0",dirs_exist_ok=True)
    shutil.copytree(f"RedLUM/data_red_lum_cpp/{dtype}/openfoam_data/constant",f"{path_case}/constant",dirs_exist_ok=True)
    shutil.copytree(f"RedLUM/data_red_lum_cpp/{dtype}/openfoam_data/system",f"{path_case}/system",dirs_exist_ok=True)


def change_param(setup,path_case):
    """
    :param key: ITHACA parameter
    :param new_value: new value to put for the given keyword
    :param path_case: test case directory
    :return:
    """

    tag_file = "RedLUM/Template/ITHACAdict0"

    name_out = f"{path_case}/system/ITHACAdict"

    # Create a copy of the tag file that is going to be modified taking the template as the model
    shutil.copyfile(tag_file, name_out)

    for key in setup.keys():
        if key in ["dtype","rtype"]:
            continue

        new_value = setup[key]

        for line in fileinput.input(name_out,inplace=True):
            # Checking if we the option to skip the line has been added
            if "skipthisline" in line:
                print(line,end="")
                continue

            if key in line:
                print(f"{key} {new_value};\n",end="")
            else:
                print(line,end="")

def save_to_json(list_of_dict,path,name):
    with open(f"{path}/{name}.json","w") as f:
       json.dump(list_of_dict,f)

def load_from_json(path,name):

    with open(f"{path}/{name}.json","r") as f:
        list_of_dict = json.load(f)

    return list_of_dict


