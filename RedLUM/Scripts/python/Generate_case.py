import numpy as np
import json
import os
import shutil
from glob import glob
import fileinput
import itertools
import utils
from tqdm import tqdm


def remove_exceptions(setup_list):
    def DNS_exceptions(setup):
        exception = False
        if "DNS" in setup["dtype"]:
            # For DNS runs the following parameter do not have any effect
            if setup["DEIMInterpolatedField"] == "nut":
                exception = True
            if setup["interpFieldCenteredOrNot"] == "1":
                exception = True
            if setup["HypRedSto"] == "1":
                exception = True
            if setup["inflatNut"] == "1":
                exception = True

        return exception

    def LES_exceptions(setup):
        exception = False
        if "LES" in setup["dtype"]:
            # HypRedSto has only a sense if DEIMInterpolatedField is on fullStressFunction
            if setup["DEIMInterpolatedField"] == "nut":
                if setup["HypRedSto"] == "1":
                    exception = True

            if "Red" in setup["rtype"]:
                exception = True

        return exception

    def DEIM_exceptions(setup):
        exception = False
        # Run with DEIM have no use with DNS data
        if "DEIM" in setup["rtype"]:
            if "DNS" in setup["dtype"]:
                exception = True

        # For the 2 following cases to work, DEIMInterpolatedField must be set to NuT
        if (("DEIMNut" in setup["rtype"]) | ("DEIMSmag" in setup["rtype"])):
            if setup["DEIMInterpolatedField"] == "fullStressFunction":
                exception = True

        return exception

    def Red_exceptions(setup):
        exception = False

        # At the moment ReducedOrderPressure is not working with the stochastic method
        if "Red" in setup["rtype"]:
            if setup["stochasOrNot"] == "1":
                exception = True

        return exception

    cleaned_setup = []
    for setup in setup_list:
        dict_setup = dict(setup)
        if DNS_exceptions(dict_setup):
            continue

        if LES_exceptions(dict_setup):
            continue

        if DEIM_exceptions(dict_setup):
            continue

        else:
            cleaned_setup.append(dict_setup)

    return cleaned_setup


if __name__ == "__main__":

    # If you want to generate ROM_ref or ROM_test cases
    case_to_generate = "ref" # test or ref

    test_setup = []

    # Test 1
    test_setup.append(dict(
        {
            "rtype": "Neg",
            "dtype": "LESRe100",
            "DEIMInterpolatedField": "fullStressFunction",
            "interpFieldCenteredOrNot": 1,
            "inflatNut": 1,
            "hilbertSpacePOD": "H1",
            "stochasOrNot": 1,
            "advModifOrNot": 1,
            "HypRedSto": 1
        }
    )
    )

    # Test 2
    test_setup.append(dict(
        {
            "rtype": "Neg",
            "dtype": "LESRe100",
            "DEIMInterpolatedField": "nut",
            "interpFieldCenteredOrNot": 1,
            "inflatNut": 1,
            "hilbertSpacePOD": "H1",
            "stochasOrNot": 1,
            "advModifOrNot": 1,
            "HypRedSto": 0
        }
    )
    )

    # Test 2
    test_setup.append(dict(
        {
            "rtype": "Neg",
            "dtype": "LESRe100",
            "DEIMInterpolatedField": "nut",
            "interpFieldCenteredOrNot": 1,
            "inflatNut": 1,
            "hilbertSpacePOD": "H1",
            "stochasOrNot": 1,
            "advModifOrNot": 1,
            "HypRedSto": 0,
            "useStratonovich":1
        }
    )
    )
    # Test 3
    test_setup.append(dict(
        {
            "rtype": "Full",
            "dtype": "DNSRe100",
            "DEIMInterpolatedField": "fullStressFunction",
            "interpFieldCenteredOrNot": 1,
            "inflatNut": 1,
            "hilbertSpacePOD": "H1",
            "stochasOrNot": 1,
            "advModifOrNot": 1,
            "HypRedSto": 1
        }
    )
    )

    # Test 4
    test_setup.append(dict(
        {
            "rtype": "Full",
            "dtype": "LESRe100",
            "DEIMInterpolatedField": "fullStressFunction",
            "interpFieldCenteredOrNot": 1,
            "inflatNut": 1,
            "hilbertSpacePOD": "L2",
            "stochasOrNot": 1,
            "advModifOrNot": 1,
            "HypRedSto": 1
        }
    ))

    # Test 5
    test_setup.append(dict(
        {
            "rtype": "Full",
            "dtype": "LESRe100",
            "DEIMInterpolatedField": "fullStressFunction",
            "interpFieldCenteredOrNot": 1,
            "inflatNut": 1,
            "hilbertSpacePOD": "H1",
            "stochasOrNot": 1,
            "advModifOrNot": 1,
            "HypRedSto": 1
        }
    ))

    # Test 6
    test_setup.append(dict(
        {
            "rtype": "Neg",
            "dtype": "DNSRe100_variableBC",
            "DEIMInterpolatedField": "fullStressFunction",
            "interpFieldCenteredOrNot": 1,
            "inflatNut": 1,
            "hilbertSpacePOD": "L2",
            "stochasOrNot": 0,
            "advModifOrNot": 1,
            "HypRedSto": 0
        }
    )
    )

    # Test 10
    test_setup.append(dict(
        {
            "rtype": "Neg",
            "dtype": "DNSRe100",
            "DEIMInterpolatedField": "fullStressFunction",
            "interpFieldCenteredOrNot": 1,
            "inflatNut": 1,
            "hilbertSpacePOD": "L2",
            "stochasOrNot": 0,
            "advModifOrNot": 1,
            "HypRedSto": 0
        }
    )
    )

    # Test 7
    test_setup.append(dict(
        {
            "rtype": "DEIM",
            "dtype": "LESRe100",
            "DEIMInterpolatedField": "fullStressFunction",
            "interpFieldCenteredOrNot": 1,
            "inflatNut": 1,
            "hilbertSpacePOD": "H1",
            "stochasOrNot": 1,
            "advModifOrNot": 1,
            "HypRedSto": 1
        }
    )
    )

    # Test 8
    test_setup.append(dict(
        {
            "rtype": "DEIMnut",
            "dtype": "LESRe100",
            "DEIMInterpolatedField": "nut",
            "interpFieldCenteredOrNot": 1,
            "inflatNut": 1,
            "hilbertSpacePOD": "H1",
            "stochasOrNot": 1,
            "advModifOrNot": 1,
            "HypRedSto": 0
        }
    )
    )


    # Test 9
    test_setup.append(dict(
        {
            "rtype": "DEIMSmag",
            "dtype": "LESRe100",
            "DEIMInterpolatedField": "nut",
            "interpFieldCenteredOrNot": 1,
            "inflatNut": 1,
            "hilbertSpacePOD": "H1",
            "stochasOrNot": 1,
            "advModifOrNot": 1,
            "HypRedSto": 0
        }
    )
    )

    np.save("RedLUM/Scripts/python/npy/test_setup",test_setup)

    utils.save_to_json(test_setup,"RedLUM/Scripts/python/json","test_setup")

    if not case_to_generate in ["test","ref"]:
        raise ValueError("case_to_generate must be either test or ref")
    for setup in tqdm(test_setup):
        dtype = setup["dtype"]
        rtype = setup["rtype"]
        dirname = utils.setup_to_dirname(setup)
        path_output = f"RedLUM/data_red_lum_cpp/{dtype}/ROM_{case_to_generate}/{rtype}/{dirname}/"
        utils.setup_dir(dtype, path_output)
        utils.change_param(setup, path_output)


    # dtype_list = ["DNSRe100","LESRe100","DNSRe100_variableBC"]
    # # Copying ROM_ref directories for tests
    # for dtype in dtype_list
    #     shutil.copytree(f"RedLUM/data_red_lum_cpp/{dtype}/ROM_ref/",
    #                     f"RedLUM/data_red_lum_cpp/{dtype}/ROM_test/",
    #                     dirs_exist_ok=True)
