import numpy as np
import os
import shutil
import fileinput
import itertools
import utils
from tqdm import tqdm


def setup_dir(dtype,path_case):
    """

    :param dtype: simulation type (DNS, LES)
    :param path_case: test case directory
    :return:
    """
    os.makedirs(path_case, exist_ok=True)
    shutil.copytree(f"RedLUM/data_red_lum_cpp/{dtype}/openfoam_data/0",f"{path_case}/0",dirs_exist_ok=True)
    shutil.copytree(f"RedLUM/data_red_lum_cpp/{dtype}/openfoam_data/constant",f"{path_case}/constant",dirs_exist_ok=True)
    shutil.copytree(f"RedLUM/data_red_lum_cpp/{dtype}/openfoam_data/system",f"{path_case}/system",dirs_exist_ok=True)

def change_param(setup,path_case):
    """
    :param key: ITHACA parameter
    :param new_value: new value to put for the given keyword
    :param path_case: test case directory
    :return:
    """

    tag_file = "/home/fregnault/red_lum_data/RedLUM/Template/ITHACAdict0"

    name_out = f"{path_case}/system/ITHACAdict"

    # Create a copy of the tag file that is going to be modified taking the template as the model
    shutil.copyfile(tag_file, name_out)

    for key in setup.keys():
        if key in ["dtype","rtype"]:
            continue

        new_value = setup[key]

        for line in fileinput.input(name_out,inplace=True):
            # Checking if we the option to skip the line has been added
            if "skipthisline" in line:
                print(line,end="")
                continue

            if key in line:
                print(f"{key} {new_value};\n",end="")
            else:
                print(line,end="")

def remove_exceptions(setup_list):

    def DNS_exceptions(setup):
        exception = False
        if "DNS" in setup["dtype"]:
            # For DNS runs the following parameter do not have any effect
            if setup["DEIMInterpolatedField"] == "nut":
                exception = True
            if setup["interpFieldCenteredOrNot"] == "1":
                exception = True
            if setup["HypRedSto"] == "1":
                exception = True
            if setup["inflatNut"] == "1":
                exception = True

        return exception

    def LES_exceptions(setup):
        exception = False
        if "LES" in setup["dtype"]:
            # HypRedSto has only a sense if DEIMInterpolatedField is on fullStressFunction
            if setup["DEIMInterpolatedField"] == "nut":
                if setup["HypRedSto"] == "1":
                    exception = True

            if "Red" in setup["rtype"]:
                exception = True

        return exception

    def DEIM_exceptions(setup):
        exception = False
        # Run with DEIM have no use with DNS data
        if "DEIM" in setup["rtype"]:
            if "DNS" in setup["dtype"]:
                exception = True

        # For the 2 following cases to work, DEIMInterpolatedField must be set to NuT
        if (("DEIMNut" in setup["rtype"])| ("DEIMSmag" in setup["rtype"])):
            if setup["DEIMInterpolatedField"] == "fullStressFunction":
                exception = True

        return exception

    def Red_exceptions(setup):
        exception = False

        # At the moment ReducedOrderPressure is not working with the stochastic method
        if "Red"  in setup["rtype"]:
            if setup["stochasOrNot"] == "1":
                exception = True

        return exception

    cleaned_setup = []
    for setup in setup_list:
        dict_setup = dict(setup)
        if DNS_exceptions(dict_setup):
            continue

        if LES_exceptions(dict_setup):
            continue

        if DEIM_exceptions(dict_setup):
            continue

        else:
            cleaned_setup.append(dict_setup)

    return cleaned_setup

if __name__ == "__main__":


    # Type of Run
    dict_param = dict({
        "dtype": ["DNSRe100", "LESRe100"],
        "rtype" : ["Neg", "Red", "Full", "DEIM", "DEIMNut", "DEIMSmag"],
        # "rtype": ["Neg"],
        "hilbertSpacePOD":["L2","H1"],
        "stochasOrNot":["0","1"],
        "advModifOrNot":["0","1"],
        "interpFieldCenteredOrNot": ["0","1"],
        "HypRedSto":["0","1"],
        "inflatNut":["0","1"],
        "DEIMInterpolatedField": ["fullStressFunction", "nut"]
    })


    list_setup = list(utils.product_dict(**dict_param))

    list_setup_cleaned = remove_exceptions(list_setup)

    np.save("RedLUM/Scripts/python/npy/list_setup_cleaned",list_setup_cleaned)
    np.save("RedLUM/Scripts/python/npy/list_setup",list_setup)

    for setup in tqdm(list_setup_cleaned):
        dtype = setup["dtype"]
        rtype = setup["rtype"]
        dirname = utils.setup_to_dirname(setup)
        path_output = f"RedLUM/data_red_lum_cpp/{dtype}/ROM_ref/{rtype}/{dirname}/"
        setup_dir(dtype,path_output)
        change_param(setup,path_output)

