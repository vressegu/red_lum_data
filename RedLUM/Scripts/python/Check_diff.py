import os
import numpy as np
import json
import utils
from dataclasses import dataclass
from typing import Tuple, List

from filecmp import dircmp

def get_diff_files(dcmp, list_of_diff_files):
    """
    Print files that are different.
    Adapted from https://docs.python.org/3/library/filecmp.html#filecmp.dircmp.report_full_closure
    :param dcmp: resultt of a dircmp command
    :return:
    """
    for name in dcmp.diff_files:
        list_of_diff_files.append(f"{dcmp.left}/{name}")

    for sub_dcmp in dcmp.subdirs.values():
        get_diff_files(sub_dcmp, list_of_diff_files)

def readBinaryFile(path) -> np.array:
    def clean_line(line):
        line = line.replace("(", "")
        line = line.replace(")", "")
        line = line.replace("+", "")
        line = line.replace("\n", "")
        line = line.split(" ")

        return line

    data=[]
    with open(path) as f:
        for lines in f.readlines():
            # Skip lines where there is no data in openfoam data files
            # and if we are not in the Matrices directory
            if (lines[0] != "(") & (not "Matrices" in path):
                pass
            else:
                lines = clean_line(lines)
                data += [float(v) for v in lines]
    return np.array(data)

@dataclass
class Error:
    name: str
    er_abs: float
    er_rel: float
    a: float
    b: float
    def __str__(self) -> str:
        return f"  rel: {self.er_rel:3.7%} abs: {self.er_abs:.16e} (ref:{self.a} test:{self.b})@ {self.name}\n"


def isGood(val_a: np.array, val_b: np.array, name) -> Tuple[bool,List[Error]]:
    val_a=val_a.ravel()
    val_b=val_b.ravel()

    abs_err = np.abs(val_b - val_a)
    rel_err = ((abs_err) / np.abs(val_a))


    ma = np.argmax(abs_err)
    mr = np.argmax(rel_err)

    if ma!=mr:
        ga,ea = isGood(np.array([val_a[ma]]),np.array([val_b[ma]]), "(ABS) "+name)
        gr,er = isGood(np.array([val_a[mr]]),np.array([val_b[mr]]), "(REL) "+name)
        return (ga and gr,ea+er)

    max_abs_err = np.abs(abs_err[ma])
    max_rel_err = np.abs(rel_err[ma])

    if (max_rel_err > 1e-14 or max_abs_err > 1e-14) and max_abs_err > 1e-16: # 1e-16 epsilone
        print("######## KO",name,"#"*100)
        return (False,[Error(name,max_abs_err,max_rel_err,val_a[ma],val_b[ma])])
    else:
        print(name,"ok...")
        return (True,[])

if __name__ == "__main__":
    report_path = "RedLUM/data_red_lum_cpp/report_diff.md"
    setup_list = utils.load_from_json("RedLUM/Scripts/python/json", "test_setup")
    dict_diff_files = dict()  # Dictionnaries containing the file where we observe find a difference for a given setup
    store_dtype_rtype = dict()  # For clarity we define a dictionnary that is going to keep dtype and rtype as it is not in the dirname variable
    allGood = True  # initialize a variable that will allow to check if everything went well in the process 0 means everything is alright

    print(f"################################ Scanning folders for differences ################################")
    for setup in setup_list:
        dtype = setup["dtype"]
        rtype = setup["rtype"]
        dirname = utils.setup_to_dirname(setup)

        key_dirname = f"{dtype}_{rtype}_{dirname}"

        ROM_ref = f"RedLUM/data_red_lum_cpp/{dtype}/ROM_ref/{rtype}/{dirname}/ITHACAoutput/"
        ROM_test = f"RedLUM/data_red_lum_cpp/{dtype}/ROM_test/{rtype}/{dirname}/ITHACAoutput/"

        dict_diff_files[key_dirname] = []

        print(f"Scanning {key_dirname} for differences")
        dcmp = dircmp(ROM_test, ROM_ref)
        get_diff_files(dcmp, dict_diff_files[key_dirname])
        if dict_diff_files[key_dirname] != 0:
            store_dtype_rtype[key_dirname] = rtype, dtype

    print(f"################################# Scanning files for differences #################################")
    with open(report_path, "w") as f:
        for setup in dict_diff_files.keys():
            setup = setup.split("/")[-1]
            dtype, rtype = store_dtype_rtype[setup]
            f.write(f"### {dtype} / {rtype} / {setup}:\n")

            errors: List[Error]=[]


            for fileB in dict_diff_files[setup]:
                extension = fileB.split(".")[-1]
                name = fileB.split("/")[-2:]
                name = "/".join(name)

                fileA = fileB.replace("ROM_test", "ROM_ref")


                if extension in ["npy"]:
                    s,r = isGood(np.load(fileA), np.load(fileB), name)
                    errors+=r
                    allGood &= s
                elif extension == "npz":
                    s,r = isGood(np.load(fileA)["data"], np.load(fileB)["data"], name)
                    errors+=r
                    allGood &= s
                else:
                    try:  # Some files inside Matrices directory without any extension are binary file. Trying to open it will raise an error so we simply skip those files
                        s,r = isGood(readBinaryFile(fileA), readBinaryFile(fileB), name)
                        errors+=r
                        allGood &= s
                    except:
                        print(f"can't analyse file: {fileB}")

            for err in sorted(errors,key=lambda x:-x.er_rel):
                f.write(str(err))        

    if allGood:
        print("ALL GOOD :) ####################################################################################")
    else:
        print("NOT OK :( ####################################################################################")
        os.system(f"cat {report_path}")
        print("NOT OK :( ####################################################################################")
        exit(1)