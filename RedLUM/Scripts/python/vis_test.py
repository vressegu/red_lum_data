import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import utils


def options_to_filename(selected_options):
    expected_options = utils.get_list_param()
    out = []
    for sel in expected_options:
        # Particular case of the spacePOD where options are L2/H1
        if sel == "pod":
            if "pod" in selected_options:
                out.append(f"{sel}_H1_")
            else:
                out.append(f"{sel}_L2_")
            continue

        # Particular case of the DEIMInterp where options are nut/fullstressfunction
        if sel == "pod":
            if "pod" in selected_options:
                out.append(f"{sel}_nut_")
            else:
                out.append(f"{sel}_L2_")
        if sel == "DEIMInt":
            if "DEIMInt" in selected_options:
                out.append(f"{sel}_nut_")
            else:
                out.append(f"{sel}_fullStressFunction_")
            continue

        if sel in selected_options:
            # if sel == "pod":
            #     to_add = "H1" if
            out.append(f"{sel}_1_")
        else:
            out.append(f"{sel}_0_")

    out = "".join(out)
    out = out[:-1]
    return out

# Sample DataFrame
data = {
    'DNS': [1, 0, 1, 0,1,0],
    'LES': [0, 1, 0, 0,1,0],
    # 'DEIM': [0, 0, 1, 0]
}
df = pd.DataFrame(data)

# Sample DataFrame
new_data = {
    'DNS': [1, 1, 1, 0,1,0],
    'LES': [1, 1, 1, 0,1,0],
    # 'DEIM': [0, 0, 1, 0]
}

new_df = pd.DataFrame(new_data)

# Initialize the Dash app
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

# Define the layout
app.layout = dbc.Container([
    dbc.Row([
        dbc.Col([
            dcc.Checklist(
                id='checkboxes',
                options=[
                    {"label": "HypRedSto ", "value": "Hyp"},
                    {"label": "StochasOrNot ", "value": "Sto"},
                    {"label": "advModifOrNot ", "value": "adv"},
                    {"label": "InterpFieldCentered ", "value": "Int"},
                    {"label": "DEIMInterpolatedField ", "value": "DEIMInt"},
                    {"label": "inflatNut ", "value": "inf"},
                    {"label": "hilbertspacepod ", "value": "pod"},
                ],
                value=[],
                inline=True
            )
        ], width=15)
    ]),
    dbc.Row([
        dbc.Col([
            dcc.Graph(id='table')
        ], width=12)
    ])
])

# Define the callback to update the table
@app.callback(
    Output('table', 'figure'),
    [Input('checkboxes', 'value')]
)
def update_table(selected_options):
    updated_df = df.copy()

    new_name = options_to_filename(selected_options)
    print(new_name)

    # load_new_data(f"{path_data}/{new_name}")

    if "Hyp" in selected_options:
        updated_df = new_df
    if 'B' in selected_options:
        updated_df['B'] *= 0
    if 'C' in selected_options:
        updated_df['C'] **= 0

    fig = go.Figure(data=[go.Table(
        header=dict(values=list(updated_df.columns),
                    fill_color='paleturquoise',
                    align='left'),
        cells=dict(values=[updated_df[col] for col in updated_df.columns],
                   fill_color='lavender',
                   align='left'))
    ])


    fig = px.imshow(updated_df, text_auto=True, aspect='auto')
    fig.update_layout(
        title='Interactive Table',
        xaxis_title='Data type',
        yaxis_title='Run type',
        yaxis=dict(
            tickmode="array",
            tickvals=list(range(len(updated_df))),
            ticktext=["Neg","Red","Full","DEIM","DEIMNut","DEIMSmag"]),
        coloraxis_showscale=False
    )
    
    return fig

# Run the app
if __name__ == '__main__':
    app.run_server(debug=True)