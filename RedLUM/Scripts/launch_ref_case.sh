#!/bin/bash

# Function to check if a string is in a dictionnary
function check_string_in_directories() {
  local search_string="$1"
  local search_directory="$2"

  # Loop through all directories in the search directory
  for dir in "$search_directory"/*/; do
    # Check if the directory name contains the search string
    if [[ "$(basename "$dir")" == *"$search_string"* ]]; then
      echo "Directory '$dir' contains the string '$search_string'."
    else
      echo "Directory '$dir' does not contain the string '$search_string'."
    fi
  done
}
# Changing the time format os that the time command shows only the real time
TIMEFORMAT=%R

pwd

cd "../data_red_lum_cpp"
for dtype in */; do
    echo "Now considering " ${dtype#/} "data"
    cd "${dtype#/}/ROM_ref"

    for exe_case in */; do
      cd $exe_case
        for param_case in */; do
          echo "Running:" $exe_case "for" $param_case
          cd $param_case
          case "$exe_case" in *"Neg"*)
            time redlum-neglectedPressure > log.txt
          esac
          case "$exe_case" in *"Red"*)
            time redlum-reducedOrderPressure > log.txt
          esac
          case "$exe_case" in *"Full"*)
            time redlum-fullOrderPressure > log.txt
          esac
          case "$exe_case" in *"DEIM"*)
            time testDEIMconvergence > log.txt
          esac
          case "$exe_case" in *"DEIMNut"*)
            time testDEIMconvergenceNut > log.txt
          esac
          case "$exe_case" in *"DEIMSmag"*)
            time testDEIMconvergenceSmagFromNut > log.txt
          esac
          cd ..
        done
      cd ..
    done

    cd ../..
done
