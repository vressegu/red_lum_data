![Static Badge](https://img.shields.io/badge/RedLUM_data-3.6.0-blue?style=for-the-badge)

This repository contains the data and a set of scripts used to test the RedLUM algorithm. 
This version of the code was to be used with RedLUM v3.3.4.

# Structure 

`data_red_lum_cpp` is the directory where all the data is stored for all the datatypes (DNS, LES, and so on) used for 
testing. Each datatype contains an `openfoam_data` directory containing the output of the corresponding OpenFOAM 
simulation required for RedLUM.

`ROM_ref` corresponds to the reference data

`ROM_test` Are directory where tests are going to be launched. .

Both `ROM_ref`and `ROM_test` can be initialised with the`Generate_case.py` with the variable `case_to_generate` that can 
be set to "test" or ref. By default, they don’t need to be.


`Scripts` contains script to launch the code and to perform analysis on the results.
```bash
├── data_red_lum_cpp
│    ├── DNSRe100
│    │   ├── openfoam_data
│    │   ├── ROM_ref
│    │   └── ROM_test
│    ├── DNSRe100_variable_BC
│    │   ├── openfoam_data
│    │   ├── ROM_ref
│    │   └── ROM_test
│    └── LESRe100
│        ├── openfoam_data
│        ├── ROM_ref
│        └── ROM_test
├── Scripts
│    ├── launch_ref_case.sh
│    └── python
└── Template
    └── ITHACAdict0
```

# Usage

## Installing the python env

The python environment can be installed using 
```
pip install -r requirements.txt
```

## Generate results to be compared with references

Make sure that you have the good version of RedLUM compiled on your computer.
Then starting from the root repo directory
```bash
cd RedLUM/Scripts
./test_RedLUM.sh
```

## Comparing to the reference

Run `Check_diff.py` with 
```
python RedLUM/Scripts/python/Check_diff.py 
```

It will scan the ROM_test and ROM_ref directories. The difference are written in a report in markdown. 
The report is stored in the `RedLUM/data_red_lum_cpp/report_diff.md` file.

## Cleaning ITHACAoutput

You might want to remove all the ITHACAOutput folders from all the test or reference folders. 
To do so run the following command from the `RedLUM/Scripts` directory :
```
./RemoveIthacaOutputs.sh [ref]
```
Specifying `ref` allows you to clean the ITHACAoutput from the reference folders, otherwise it cleans the test folders.


## Generate new reference data

Make sure that you have the version of the code that you want to use to generate the new reference case compiled on 
your computer then do the following
``` bash
cd RedLUM/Scripts/
bash Generate_ref_data.sh [tag_id]
```
with [tag_id] the name of the tag (typically the RedLUM version)

## Generate new reference data (old way)

Make sure that you have the version of the code that you want to use to generate the new reference case compiled on 
your computer then do the following

Move to the directory containing the scripts
``` bash
cd red_lum_data/Scripts/
```
Remove all the existing ITHACAOutput in the `ROM_ref` folders
```bash
./RemoveIthacaOutputs.sh ref 
```
Execute RedLUM and populate ROM_ref folders
```bash
./test_RedLUM.sh ref 
```

## Adding new tests 

All the testing configurations are written in the main part of the `Generate_case.py` file. 

Here is one example 

```python
test = dict(
    {
        "rtype": "Neg",
        "dtype": "LESRe100",
        "DEIMInterpolatedField": "fullStressFunction",
        "interpFieldCenteredOrNot": 1,
        "inflatNut": 1,
        "hilbertSpacePOD": "H1",
        "stochasOrNot": 1,
        "advModifOrNot": 1,
        "HypRedSto": 1
    }
    )
```

Each test configuration is a dictionary stored in the `test_setup` list with the keywords being the different parameters
specified in the `Ithacadict` file. 

To add a new test you then have to append a new dictionary like above to the test_setup list. 

__/!\ Important__

__If you want to add a new parameter you need to make sure that this parameter is in the `RedLUM/Template/Ithacadict0` 
file. Moreover, you need to modify the `setup_to_dirname.py` function in `RedLUM/Scripts/python/utils.py` so that 
folder names contain the parameter you just added.__

Finally, launch the `Generate_case.py` file. 
```bash
python RedLUM/Scripts/python/Generate_case.py 
```
In this file you need to change the value of the `case_to_generate` variable to `"ref"` or `"test"` to create the 
reference and testing data.
